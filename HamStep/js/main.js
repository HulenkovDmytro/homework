"use strict";

// Our Services

const tabsBtnServices = document.querySelectorAll(".services__tabs-title");
const tabsItems = document.querySelectorAll(".services__tabs-item");

tabsBtnServices.forEach(function (title) {
  title.addEventListener("click", function () {
    let thisBtn = title;
    let thisId = thisBtn.getAttribute("data-tab");
    let currentTab = document.querySelector(thisId);

    if (!thisBtn.classList.contains("active")) {
      tabsBtnServices.forEach(function (title) {
        title.classList.remove('active');
      })

      tabsItems.forEach(function (title) {
        title.classList.remove('active');
      })

      thisBtn.classList.add('active');
      currentTab.classList.add("active");
    }
  })
})

// Our works

let amTitle = document.querySelectorAll(".works__tabs-title");
let amContent = document.querySelectorAll(".works__tabs");
let amTarget;
let loadMore = document.querySelector(".plus");

loadMore.addEventListener("click", () => {
  loadMore.style.display = "none";
  console.log(loadMore);
  let target = document.querySelector(".works__tabs-title.active").getAttribute("data-target");
  amContent.forEach((e) => {
    if (target == "target-all") {
      e.classList.add("active");
    } else {
      if (e.classList.contains(target)) {
        e.classList.add("active");
      } else e.classList.remove("active");
    }
  });
});

amTitle.forEach(function (amclick) {
  amclick.addEventListener("click", (e) => {
    loadMore.style.display = "block";
    amTitle.forEach((amclick) => {
      amclick.classList.remove("active");
    });
    amclick.classList.add("active");
    amTarget = amclick.getAttribute("data-target");
    changeAmContent(amTarget);
  });
});

function changeAmContent(amTarget) {
  let counter = 0;
  amContent.forEach((e) => {
    if (amTarget == "target-all") {
      if (counter < 12) {
        e.classList.add("active");
      } else e.classList.remove("active");
      counter++;
    } else {
      if (e.classList.contains(amTarget)) {
        if (counter < 4) {
          e.classList.add("active");
        } else e.classList.remove("active");
        counter++;
      } else e.classList.remove("active");
    }
  });
}

// About

let aboutTitle = document.querySelectorAll(".about__tabs-item");
let aboutContent = document.querySelectorAll(".about__content-item");
let aboutTarget;

aboutTitle.forEach(function (aboutclick) {
  aboutclick.addEventListener("click", (e) => {
    aboutTitle.forEach((aboutclick) => {
      aboutclick.classList.remove("active");
    });
    aboutclick.classList.add("active");
    aboutTarget = aboutclick.getAttribute("data-target");
    currentSlide = aboutTarget.replace("target-", "");
    currentSlide--;
    changeAboutContent(aboutTarget);
  });
});

function changeAboutContent(aboutTarget) {
  aboutContent.forEach((e) => {
    if (e.classList.contains(aboutTarget)) {
      e.classList.add("active");
    } else e.classList.remove("active");
  });
}

// Prev & Next buttons

const slides = document.querySelectorAll(".about__tabs .about__tabs-item");
let currentSlide = 0;
const btnBack = document.getElementById("about__back");
btnBack.addEventListener("click", back);
const btnNext = document.getElementById("about__next");
btnNext.addEventListener("click", next);

function back() {
  slides[currentSlide].className = "about__tabs-item";
  if (currentSlide == 0) {
    currentSlide = 3;
    slides[currentSlide].className = "about__tabs-item active";
    aboutTarget = slides[currentSlide].getAttribute("data-target");
    changeAboutContent(aboutTarget);
  } else {
    currentSlide = (currentSlide - 1) % slides.length;
    slides[currentSlide].className = "about__tabs-item active";
    aboutTarget = slides[currentSlide].getAttribute("data-target");
    changeAboutContent(aboutTarget);
  }
}

function next() {
  aboutTitle.forEach((aboutclick) => {
    aboutclick.classList.remove("active");
  });
  if (currentSlide == 4) {
    currentSlide = 0;
    slides[currentSlide].className = "about__tabs-item active";
    aboutTarget = slides[currentSlide].getAttribute("data-target");

    changeAboutContent(aboutTarget);
  } else {
    currentSlide++;
    if (currentSlide == 4) {
      currentSlide = 0;
    }
    slides[currentSlide].className = "about__tabs-item active";
    aboutTarget = slides[currentSlide].getAttribute("data-target");

    changeAboutContent(aboutTarget);
  }
}